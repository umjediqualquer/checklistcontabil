<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index'])->middleware('verified');


// Companies
Route::group(['prefix' => 'companies', 'middleware' => 'role:super_admin'], function () {
    Route::get('/create',                                   ['as'=>'companies.create',  'uses'=>'CompanyController@create']);
    Route::post('/',                                        ['as'=>'companies.store',   'uses'=>'CompanyController@store']);
    Route::delete('/{company_id}',                          ['as'=>'companies.destroy', 'uses'=>'CompanyController@destroy']);
});

Route::group(['prefix' => 'companies', 'middleware' => 'role:super_admin|admin'], function () {
    Route::get('/',                                         ['as'=>'companies.index',   'uses'=>'CompanyController@index']);
    Route::get('/{company_id}',                             ['as'=>'companies.show',    'uses'=>'CompanyController@show']);
    Route::match(['put', 'patch'], '/{company_id}',         ['as'=>'companies.update',  'uses'=>'CompanyController@update']);
    Route::get('/{company_id}/edit',                        ['as'=>'companies.edit',    'uses'=>'CompanyController@edit']);
});

// Clients
Route::group(['prefix' => 'clients', 'middleware' => 'role:super_admin|admin'], function () {
    Route::get('/create',                                   ['as'=>'clients.create',  'uses'=>'ClientController@create']);
    Route::post('/',                                        ['as'=>'clients.store',   'uses'=>'ClientController@store']);
    Route::delete('/{client_id}',                           ['as'=>'clients.destroy', 'uses'=>'ClientController@destroy']);
});

Route::group(['prefix' => 'clients', 'middleware' => 'role:super_admin|admin|client'], function () {
    Route::get('/',                                         ['as'=>'clients.index',   'uses'=>'ClientController@index']);
    Route::get('/{client_id}',                              ['as'=>'clients.show',    'uses'=>'ClientController@show']);
    Route::match(['put', 'patch'], '/{client_id}',          ['as'=>'clients.update',  'uses'=>'ClientController@update']);
    Route::get('/{client_id}/edit',                         ['as'=>'clients.edit',    'uses'=>'ClientController@edit']);
});

// Groups
Route::group(['prefix' => 'groups', 'middleware' => 'role:super_admin|admin'], function () {
    Route::get('/',                                         ['as'=>'groups.index',   'uses'=>'GroupController@index']);
    Route::get('/create',                                   ['as'=>'groups.create',  'uses'=>'GroupController@create']);
    Route::post('/',                                        ['as'=>'groups.store',   'uses'=>'GroupController@store']);
    Route::get('/{group_id}',                               ['as'=>'groups.show',    'uses'=>'GroupController@show']);
    Route::match(['put', 'patch'], '/{group_id}',           ['as'=>'groups.update',  'uses'=>'GroupController@update']);
    Route::delete('/{group_id}',                            ['as'=>'groups.destroy', 'uses'=>'GroupController@destroy']);
    Route::get('/{group_id}/edit',                          ['as'=>'groups.edit',    'uses'=>'GroupController@edit']);
});

// Client groups
Route::group(['prefix' => 'client-groups', 'middleware' => 'role:super_admin|admin'], function () {
    Route::get('/',                                             ['as'=>'clientGroups.index',   'uses'=>'ClientGroupController@index']);
    Route::get('/create',                                       ['as'=>'clientGroups.create',  'uses'=>'ClientGroupController@create']);
    Route::post('/',                                            ['as'=>'clientGroups.store',   'uses'=>'ClientGroupController@store']);
    Route::get('/{clientGroup_id}',                             ['as'=>'clientGroups.show',    'uses'=>'ClientGroupController@show']);
    Route::match(['put', 'patch'], '/{clientGroup_id}',         ['as'=>'clientGroups.update',  'uses'=>'ClientGroupController@update']);
    Route::delete('/{clientGroup_id}',                          ['as'=>'clientGroups.destroy', 'uses'=>'ClientGroupController@destroy']);
    Route::get('/{clientGroup_id}/edit',                        ['as'=>'clientGroups.edit',    'uses'=>'ClientGroupController@edit']);
});

// Processes
Route::group(['prefix' => 'processes', 'middleware' => 'role:super_admin|admin'], function () {
    Route::get('/',                                         ['as'=>'processes.index',   'uses'=>'ProcessController@index']);
    Route::get('/create',                                   ['as'=>'processes.create',  'uses'=>'ProcessController@create']);
    Route::post('/',                                        ['as'=>'processes.store',   'uses'=>'ProcessController@store']);
    Route::get('/{process_id}',                             ['as'=>'processes.show',    'uses'=>'ProcessController@show']);
    Route::match(['put', 'patch'], '/{process_id}',         ['as'=>'processes.update',  'uses'=>'ProcessController@update']);
    Route::delete('/{process_id}',                          ['as'=>'processes.destroy', 'uses'=>'ProcessController@destroy']);
    Route::get('/{process_id}/edit',                        ['as'=>'processes.edit',    'uses'=>'ProcessController@edit']);
});

// Executions
Route::group(['prefix' => 'executions', 'middleware' => 'role:super_admin|admin|client'], function () {
    Route::get('/',                                           ['as'=>'executions.index',   'uses'=>'ExecutionController@index']);
    Route::get('/create',                                     ['as'=>'executions.create',  'uses'=>'ExecutionController@create']);
    Route::post('/',                                          ['as'=>'executions.store',   'uses'=>'ExecutionController@store']);
    Route::get('/{execution_id}',                             ['as'=>'executions.show',    'uses'=>'ExecutionController@show']);
    Route::match(['put', 'patch'], '/{execution_id}',         ['as'=>'executions.update',  'uses'=>'ExecutionController@update']);
    Route::delete('/{execution_id}',                          ['as'=>'executions.destroy', 'uses'=>'ExecutionController@destroy']);
    Route::get('/{execution_id}/edit',                        ['as'=>'executions.edit',    'uses'=>'ExecutionController@edit']);
});

// Attachments
Route::group(['prefix' => 'attachments', 'middleware' => 'role:super_admin|admin|client'], function () {
    Route::get('/',                                            ['as'=>'attachments.index',   'uses'=>'AttachmentController@index']);
    Route::get('/create',                                      ['as'=>'attachments.create',  'uses'=>'AttachmentController@create']);
    Route::post('/',                                           ['as'=>'attachments.store',   'uses'=>'AttachmentController@store']);
    Route::get('/{attachment_id}',                             ['as'=>'attachments.show',    'uses'=>'AttachmentController@show']);
    Route::match(['put', 'patch'], '/{attachment_id}',         ['as'=>'attachments.update',  'uses'=>'AttachmentController@update']);
    Route::delete('/{attachment_id}',                          ['as'=>'attachments.destroy', 'uses'=>'AttachmentController@destroy']);
    Route::get('/{attachment_id}/edit',                        ['as'=>'attachments.edit',    'uses'=>'AttachmentController@edit']);
});

Route::group(['prefix' => 'clients/{client_id}', 'middleware' => 'check.client'], function() {
    Route::group(['prefix' => 'checklist'], function() {
        Route::get('/',                                        ['as'=>'clients.checklist', 'uses'=>'ClientController@checklist']);
    });

    Route::group(['prefix' => 'processes'], function() {
        Route::get('/{process_id}/execution/{execution_id}/toggle',                      ['as'=>'processes.toggleIsReady',   'uses'=>'ProcessController@toggleIsReady']);
        Route::get('/{process_id}/execute',                                              ['as'=>'processes.createExecution', 'uses'=>'ProcessController@createExecution']);
    });
});