<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ClientGroup
 * @package App\Models
 * @version January 28, 2020, 3:00 pm UTC
 *
 * @property \App\Models\Client client
 * @property \App\Models\Group group
 * @property integer client_id
 * @property integer group_id
 */
class ClientGroup extends Model
{

    public $table = 'client_groups';
    



    public $fillable = [
        'client_id',
        'group_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'client_id' => 'integer',
        'group_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'client_id' => 'required',
        'group_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class, 'group_id', 'id');
    }
}
