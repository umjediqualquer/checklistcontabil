<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Client
 * @package App\Models
 * @version January 28, 2020, 2:59 pm UTC
 *
 * @property \App\Models\Company company
 * @property integer company_id
 * @property string name
 * @property string|\Carbon\Carbon birth_date
 * @property string rg
 * @property string phone
 * @property string address
 */
class Client extends Model
{

    public $table = 'clients';




    public $fillable = [
        'company_id',
        'name',
        'birth_date',
        'rg',
        'phone',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'name' => 'string',
        'birth_date' => 'datetime',
        'rg' => 'string',
        'phone' => 'string',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'company_id' => 'required',
        'name' => 'required',
        'birth_date' => 'required',
        'rg' => 'required',
        'phone' => 'required',
        'address' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id', 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(\App\Models\Group::class, 'client_groups', 'client_id', 'group_id');
    }
}
