<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Company
 * @package App\Models
 * @version January 28, 2020, 2:58 pm UTC
 *
 * @property string name
 * @property string address
 * @property string cnpj
 * @property string phone
 * @property string email
 */
class Company extends Model
{

    public $table = 'companies';




    public $fillable = [
        'name',
        'address',
        'cnpj',
        'phone',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'cnpj' => 'string',
        'phone' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'address' => 'required',
        'cnpj' => 'required',
        'phone' => 'required',
        'email' => 'required'
    ];


}
