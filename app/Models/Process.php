<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Process
 * @package App\Models
 * @version January 28, 2020, 3:00 pm UTC
 *
 * @property \App\Models\Group group
 * @property integer group_id
 * @property string name
 */
class Process extends Model
{

    public $table = 'processes';




    public $fillable = [
        'group_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'group_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'group_id' => 'required',
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class, 'group_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function executions()
    {
        return $this->hasMany(\App\Models\Execution::class, 'process_id');
    }
}
