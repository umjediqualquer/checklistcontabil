<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Execution
 * @package App\Models
 * @version January 28, 2020, 3:01 pm UTC
 *
 * @property \App\Models\Client client
 * @property \App\Models\Process process
 * @property integer client_id
 * @property integer process_id
 * @property boolean is_ready
 */
class Execution extends Model implements HasMedia
{
    use HasMediaTrait;

    public $table = 'executions';




    public $fillable = [
        'client_id',
        'process_id',
        'is_ready',
        'deadline'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'client_id' => 'integer',
        'process_id' => 'integer',
        'is_ready' => 'boolean',
        'deadline' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'client_id' => 'required',
        'process_id' => 'required',
        'is_ready' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function process()
    {
        return $this->belongsTo(\App\Models\Process::class, 'process_id', 'id');
    }
}
