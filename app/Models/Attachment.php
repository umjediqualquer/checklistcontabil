<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Attachment
 * @package App\Models
 * @version January 28, 2020, 3:01 pm UTC
 *
 * @property \App\Models\Execution execution
 * @property integer execution_id
 * @property string name
 * @property string description
 * @property string path
 */
class Attachment extends Model
{

    public $table = 'attachments';
    



    public $fillable = [
        'execution_id',
        'name',
        'description',
        'path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'execution_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'execution_id' => 'required',
        'name' => 'required',
        'path' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function execution()
    {
        return $this->belongsTo(\App\Models\Execution::class, 'execution_id', 'id');
    }
}
