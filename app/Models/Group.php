<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Group
 * @package App\Models
 * @version January 28, 2020, 2:59 pm UTC
 *
 * @property \App\Models\Company company
 * @property integer company_id
 * @property string name
 * @property boolean is_unique
 */
class Group extends Model
{

    public $table = 'groups';
    



    public $fillable = [
        'company_id',
        'name',
        'is_unique'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'name' => 'string',
        'is_unique' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'company_id' => 'required',
        'name' => 'required',
        'is_unique' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id', 'id');
    }
}
