<?php

namespace App\Helpers;

class Functions {
    public static function addTimestamp($data)
        {
            return array_merge($data, ['created_at' => \DB::raw('CURRENT_TIMESTAMP'), 'updated_at' => \DB::raw('CURRENT_TIMESTAMP')]);
        }
}