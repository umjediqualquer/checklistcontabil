<?php

namespace App\DataTables;

use App\Models\Process;
use App\Models\Group;
use Yajra\DataTables\Services\DataTable;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;

class ProcessDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        if(\Auth::user()->hasRole('super_admin')) {
            $processes = Process::select();
        } else if (\Auth::user()->hasRole('admin')) {
            $groupIds = Group::select()->where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
            $processes = Process::select()->whereIn('group_id', $groupIds);
        }

        return DataTables::of($processes)
            ->addColumn('action', 'processes.datatables_actions');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'group_id',
            'name'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'processesdatatable_' . time();
    }
}
