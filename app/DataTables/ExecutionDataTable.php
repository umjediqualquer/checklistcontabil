<?php

namespace App\DataTables;

use App\Models\Execution;
use App\Models\Client;
use Yajra\DataTables\Services\DataTable;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;

class ExecutionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        if(\Auth::user()->hasRole('super_admin')) {
            $executions = Execution::select();
        } else if(\Auth::user()->hasRole('admin')) {
            $clientIds = Client::select()->where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();

            $executions = Execution::select()->whereIn('client_id', $clientIds);
        }

        return DataTables::of($executions)->addColumn('action', 'executions.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Execution $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Execution $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'client_id',
            'process_id',
            'is_ready'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'executionsdatatable_' . time();
    }
}
