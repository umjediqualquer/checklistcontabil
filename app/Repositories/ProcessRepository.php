<?php

namespace App\Repositories;

use App\Models\Process;
use App\Repositories\BaseRepository;

/**
 * Class ProcessRepository
 * @package App\Repositories
 * @version January 28, 2020, 3:00 pm UTC
*/

class ProcessRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'group_id',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Process::class;
    }
}
