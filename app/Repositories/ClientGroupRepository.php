<?php

namespace App\Repositories;

use App\Models\ClientGroup;
use App\Repositories\BaseRepository;

/**
 * Class ClientGroupRepository
 * @package App\Repositories
 * @version January 28, 2020, 3:00 pm UTC
*/

class ClientGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'group_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClientGroup::class;
    }
}
