<?php

namespace App\Repositories;

use App\Models\Execution;
use App\Repositories\BaseRepository;

/**
 * Class ExecutionRepository
 * @package App\Repositories
 * @version January 28, 2020, 3:01 pm UTC
*/

class ExecutionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'process_id',
        'is_ready'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Execution::class;
    }
}
