<?php

namespace App\Repositories;

use App\Models\Attachment;
use App\Repositories\BaseRepository;

/**
 * Class AttachmentRepository
 * @package App\Repositories
 * @version January 28, 2020, 3:01 pm UTC
*/

class AttachmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'execution_id',
        'name',
        'description',
        'path'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attachment::class;
    }
}
