<?php

namespace App\Http\Controllers;

use App\DataTables\ClientGroupDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClientGroupRequest;
use App\Http\Requests\UpdateClientGroupRequest;
use App\Repositories\ClientGroupRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Client;
use App\Models\Group;

class ClientGroupController extends AppBaseController
{
    /** @var  ClientGroupRepository */
    private $clientGroupRepository;

    public function __construct(ClientGroupRepository $clientGroupRepo)
    {
        $this->clientGroupRepository = $clientGroupRepo;
    }

    /**
     * Display a listing of the ClientGroup.
     *
     * @param ClientGroupDataTable $clientGroupDataTable
     * @return Response
     */
    public function index(ClientGroupDataTable $clientGroupDataTable)
    {
        return $clientGroupDataTable->render('client_groups.index');
    }

    /**
     * Show the form for creating a new ClientGroup.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = \Auth::user();

        if ($currentUser->hasRole('admin')) {
            $clients = Client::where('company_id', $currentUser->company_id)->get()->pluck('name', 'id')->toArray();
            $groups = Group::where('company_id', $currentUser->company_id)->get()->pluck('name', 'id')->toArray();
        } else {
            $clients = Client::all()->pluck('name', 'id')->toArray();
            $groups = Group::all()->pluck('name', 'id')->toArray();
        }
        return view('client_groups.create', compact('clients', 'groups'));
    }

    /**
     * Store a newly created ClientGroup in storage.
     *
     * @param CreateClientGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateClientGroupRequest $request)
    {
        $currentUser = \Auth::user();
        $input = $request->all();
        $client = Client::find($input['client_id']);
        $group = Group::find($input['group_id']);

        $clientGroup = $this->clientGroupRepository->create($input);

        if($currentUser->hasRole('admin')
            && ($client->company_id != $currentUser->company_id || $group->company_id != $currentUser->company_id))
        {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clientGroups.index'));
        }

        Flash::success('Client Group saved successfully.');

        return redirect(route('clientGroups.index'));
    }

    /**
     * Display the specified ClientGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $currentUser = \Auth::user();
        $clientGroup = $this->clientGroupRepository->find($id);
        $client = Client::find($clientGroup->client_id);
        $group = Group::find($clientGroup->group_id);

        if (empty($clientGroup)) {
            Flash::error('Client Group not found');

            return redirect(route('clientGroups.index'));
        }

        if($currentUser->hasRole('admin')
            && ($client->company_id != $currentUser->company_id || $group->company_id != $currentUser->company_id))
        {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clientGroups.index'));
        }

        return view('client_groups.show')->with('clientGroup', $clientGroup);
    }

    /**
     * Show the form for editing the specified ClientGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $currentUser = \Auth::user();
        $clientGroup = $this->clientGroupRepository->find($id);
        $client = Client::find($clientGroup->client_id);
        $group = Group::find($clientGroup->group_id);

        $clients = Client::all()->pluck('name', 'id')->toArray();
        $groups = Group::all()->pluck('name', 'id')->toArray();

        if (empty($clientGroup)) {
            Flash::error('Client Group not found');

            return redirect(route('clientGroups.index'));
        }

        if($currentUser->hasRole('admin')
            && ($client->company_id != $currentUser->company_id || $group->company_id != $currentUser->company_id))
        {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clientGroups.index'));
        }

        return view('client_groups.edit', compact('clients', 'groups'))->with('clientGroup', $clientGroup);
    }

    /**
     * Update the specified ClientGroup in storage.
     *
     * @param  int              $id
     * @param UpdateClientGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientGroupRequest $request)
    {
        $currentUser = \Auth::user();
        $clientGroup = $this->clientGroupRepository->find($id);
        $client = Client::find($clientGroup->client_id);
        $group = Group::find($clientGroup->group_id);

        if (empty($clientGroup)) {
            Flash::error('Client Group not found');

            return redirect(route('clientGroups.index'));
        }

        if($currentUser->hasRole('admin')
            && ($client->company_id != $currentUser->company_id || $group->company_id != $currentUser->company_id))
        {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clientGroups.index'));
        }

        $clientGroup = $this->clientGroupRepository->update($request->all(), $id);

        Flash::success('Client Group updated successfully.');

        return redirect(route('clientGroups.index'));
    }

    /**
     * Remove the specified ClientGroup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $currentUser = \Auth::user();
        $clientGroup = $this->clientGroupRepository->find($id);
        $client = Client::find($clientGroup->client_id);
        $group = Group::find($clientGroup->group_id);

        if (empty($clientGroup)) {
            Flash::error('Client Group not found');

            return redirect(route('clientGroups.index'));
        }

        if($currentUser->hasRole('admin')
            && ($client->company_id != $currentUser->company_id || $group->company_id != $currentUser->company_id))
        {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clientGroups.index'));
        }

        $this->clientGroupRepository->delete($id);

        Flash::success('Client Group deleted successfully.');

        return redirect(route('clientGroups.index'));
    }
}
