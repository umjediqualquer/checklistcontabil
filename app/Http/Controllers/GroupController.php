<?php

namespace App\Http\Controllers;

use App\DataTables\GroupDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use App\Repositories\GroupRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Company;

class GroupController extends AppBaseController
{
    /** @var  GroupRepository */
    private $groupRepository;

    public function __construct(GroupRepository $groupRepo)
    {
        $this->groupRepository = $groupRepo;
    }

    /**
     * Display a listing of the Group.
     *
     * @param GroupDataTable $groupDataTable
     * @return Response
     */
    public function index(GroupDataTable $groupDataTable)
    {
        return $groupDataTable->render('groups.index');
    }

    /**
     * Show the form for creating a new Group.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = \Auth::user();
        if ($currentUser->hasRole('admin')) {
            $companies = Company::where('id', $currentUser->company_id)->pluck('name', 'id')->toArray();
        } else {
            $companies = Company::all()->pluck('name', 'id')->toArray();
        }

        return view('groups.create', compact('companies'));
    }

    /**
     * Store a newly created Group in storage.
     *
     * @param CreateGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupRequest $request)
    {
        $currentUser = \Auth::user();
        $input = $request->all();

        $group = $this->groupRepository->create($input);

        if (($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        Flash::success('Group saved successfully.');

        return redirect(route('groups.index'));
    }

    /**
     * Display the specified Group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $currentUser = \Auth::user();
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        if (($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        return view('groups.show')->with('group', $group);
    }

    /**
     * Show the form for editing the specified Group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $currentUser = \Auth::user();
        $group = $this->groupRepository->find($id);

        $companies = Company::all()->pluck('name', 'id')->toArray();

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        if (($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        return view('groups.edit', compact('companies'))->with('group', $group);
    }

    /**
     * Update the specified Group in storage.
     *
     * @param  int              $id
     * @param UpdateGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupRequest $request)
    {
        $currentUser = \Auth::user();
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        if (($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        $group = $this->groupRepository->update($request->all(), $id);

        Flash::success('Group updated successfully.');

        return redirect(route('groups.index'));
    }

    /**
     * Remove the specified Group from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $currentUser = \Auth::user();
        $group = $this->groupRepository->find($id);

        if (empty($group)) {
            Flash::error('Group not found');

            return redirect(route('groups.index'));
        }

        if (($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        $this->groupRepository->delete($id);

        Flash::success('Group deleted successfully.');

        return redirect(route('groups.index'));
    }
}
