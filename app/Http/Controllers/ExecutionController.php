<?php

namespace App\Http\Controllers;

use App\DataTables\ExecutionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateExecutionRequest;
use App\Http\Requests\UpdateExecutionRequest;
use App\Repositories\ExecutionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Client;
use App\Models\Process;
use App\Models\ClientGroup;
use App\Models\Group;

class ExecutionController extends AppBaseController
{
    /** @var  ExecutionRepository */
    private $executionRepository;

    public function __construct(ExecutionRepository $executionRepo)
    {
        $this->executionRepository = $executionRepo;
    }

    /**
     * Display a listing of the Execution.
     *
     * @param ExecutionDataTable $executionDataTable
     * @return Response
     */
    public function index(ExecutionDataTable $executionDataTable)
    {
        return $executionDataTable->render('executions.index');
    }

    /**
     * Show the form for creating a new Execution.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = \Auth::user();
        if ($currentUser->hasRole('client')) {
            $clients = Client::where('id', $currentUser->client_id)->pluck('name', 'id')->toArray();
            $client = Client::find($currentUser->client_id);

            $groups = $client->groups;

            $processes = Process::whereIn('group_id', $groups)->pluck('name', 'id')->toArray();
        } else if ($currentUser->hasRole('admin')) {
            $clients = Client::where('company_id', $currentUser->company_id)->pluck('name', 'id')->toArray();

            $clientIds = Client::select('id')->where('company_id', $currentUser->company_id)->toArray();
            $cgGroups = ClientGroup::select('group_id')->where('client_id', $clientIds)->toArray();
            $groups = Group::select('group_id')->whereIn('id', $cgGroups)->where('company_id', $currentUser->company_id)->toArray();

            $processes = Process::whereIn('group_id', $groups)->pluck('name', 'id')->toArray();
        } else {
            $clients = Client::all()->pluck('name', 'id')->toArray();
            $processes = Process::all()->pluck('name', 'id')->toArray();
        }

        return view('executions.create', compact('clients', 'processes'));
    }

    /**
     * Store a newly created Execution in storage.
     *
     * @param CreateExecutionRequest $request
     *
     * @return Response
     */
    public function store(CreateExecutionRequest $request)
    {
        $input = $request->all();

        $execution = $this->executionRepository->create($input);

        Flash::success('Execution saved successfully.');

        return redirect(route('executions.index'));
    }

    /**
     * Display the specified Execution.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $execution = $this->executionRepository->find($id);

        if (empty($execution)) {
            Flash::error('Execution not found');

            return redirect(route('executions.index'));
        }

        return view('executions.show')->with('execution', $execution);
    }

    /**
     * Show the form for editing the specified Execution.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $execution = $this->executionRepository->find($id);
        $clients = Client::all()->pluck('name', 'id')->toArray();
        $processes = Process::all()->pluck('name', 'id')->toArray();

        if (empty($execution)) {
            Flash::error('Execution not found');

            return redirect(route('executions.index'));
        }

        return view('executions.edit', compact('clients', 'processes'))->with('execution', $execution);
    }

    /**
     * Update the specified Execution in storage.
     *
     * @param  int              $id
     * @param UpdateExecutionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExecutionRequest $request)
    {
        $execution = $this->executionRepository->find($id);

        if (empty($execution)) {
            Flash::error('Execution not found');

            return redirect(route('executions.index'));
        }

        $execution = $this->executionRepository->update($request->all(), $id);

        Flash::success('Execution updated successfully.');

        return redirect(route('executions.index'));
    }

    /**
     * Remove the specified Execution from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $execution = $this->executionRepository->find($id);

        if (empty($execution)) {
            Flash::error('Execution not found');

            return redirect(route('executions.index'));
        }

        $this->executionRepository->delete($id);

        Flash::success('Execution deleted successfully.');

        return redirect(route('executions.index'));
    }
}
