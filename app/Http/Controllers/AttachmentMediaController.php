<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Attachment;
use PhpParser\Node\Expr\New_;
use DB;
class AttachmentMediaController extends Controller
{
    /**
     * PDF Upload Code
     *
     * @return void
     */
    public function productAttachmentsStore(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        $media = $product->addMedia($request->file('file'))->toMediaCollection('attachments');
        $attachment = new Attachment();
        $attachment->product_id = $product_id;
        $attachment->name = $media->name . '.pdf';
        $attachment->path = $media->getUrl();
        $attachment->is_public = false;
        $attachment->save();
    }

    public function productAttachmentDelete($product_id, $image_id)
    {
        $product = Product::find($product_id);
        $attachment = $product->getMedia('attachments')->where('id', $image_id)->first();
        DB::table('attachments')->where('path', $attachment->getUrl())->delete();
        $attachment->delete();
        return redirect()->back();
    }
}