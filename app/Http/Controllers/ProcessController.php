<?php

namespace App\Http\Controllers;

use App\DataTables\ProcessDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProcessRequest;
use App\Http\Requests\UpdateProcessRequest;
use App\Repositories\ProcessRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Group;
use App\Models\Execution;
use Carbon\Carbon;

class ProcessController extends AppBaseController
{
    /** @var  ProcessRepository */
    private $processRepository;

    public function __construct(ProcessRepository $processRepo)
    {
        $this->processRepository = $processRepo;
    }

    /**
     * Display a listing of the Process.
     *
     * @param ProcessDataTable $processDataTable
     * @return Response
     */
    public function index(ProcessDataTable $processDataTable)
    {
        return $processDataTable->render('processes.index');
    }

    /**
     * Show the form for creating a new Process.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = \Auth::user();

        if($currentUser->hasRole('admin')) {
            $groups = Group::where('company_id', $currentUser->company_id)->get()->pluck('name', 'id')->toArray();
        } else {
            $groups = Group::all()->pluck('name', 'id')->toArray();
        }

        return view('processes.create', compact('groups'));
    }

    /**
     * Store a newly created Process in storage.
     *
     * @param CreateProcessRequest $request
     *
     * @return Response
     */
    public function store(CreateProcessRequest $request)
    {
        $currentUser = \Auth::user();
        $input = $request->all();
        $group = Group::find($input['group_id']);

        if($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('processes.index'));
        }

        $process = $this->processRepository->create($input);

        Flash::success('Process saved successfully.');

        return redirect(route('processes.index'));
    }

    /**
     * Display the specified Process.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $currentUser = \Auth::user();
        $process = $this->processRepository->find($id);
        $group = Group::find($process->group_id);

        if (empty($process)) {
            Flash::error('Process not found');

            return redirect(route('processes.index'));
        }

        if($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('processes.index'));
        }

        return view('processes.show')->with('process', $process);
    }

    /**
     * Show the form for editing the specified Process.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $currentUser = \Auth::user();
        $process = $this->processRepository->find($id);
        $group = Group::find($process->group_id);

        $groups = Group::all()->pluck('name', 'id')->toArray();

        if (empty($process)) {
            Flash::error('Process not found');

            return redirect(route('processes.index'));
        }

        if($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('processes.index'));
        }

        return view('processes.edit', compact('groups'))->with('process', $process);
    }

    /**
     * Update the specified Process in storage.
     *
     * @param  int              $id
     * @param UpdateProcessRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProcessRequest $request)
    {
        $currentUser = \Auth::user();
        $process = $this->processRepository->find($id);
        $group = Group::find($process->group_id);

        if (empty($process)) {
            Flash::error('Process not found');

            return redirect(route('processes.index'));
        }

        if($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('processes.index'));
        }

        $process = $this->processRepository->update($request->all(), $id);

        Flash::success('Process updated successfully.');

        return redirect(route('processes.index'));
    }

    /**
     * Remove the specified Process from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $currentUser = \Auth::user();
        $process = $this->processRepository->find($id);
        $group = Group::find($process->group_id);

        if (empty($process)) {
            Flash::error('Process not found');

            return redirect(route('processes.index'));
        }

        if($currentUser->hasRole('admin') && $group->company_id != $currentUser->company_id) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('processes.index'));
        }

        $this->processRepository->delete($id);

        Flash::success('Process deleted successfully.');

        return redirect(route('processes.index'));
    }

    public function toggleIsReady($id)
    {
        $execution = Execution::find(request()->execution_id);
        $execution->is_ready = !$execution->is_ready;
        $execution->deadline = Carbon::parse(request()->deadline)->endOfMonth();
        $execution->save();
    }

    public function createExecution()
    {
        $execution = Execution::create([
            'client_id' => request()->get('current-client')->id,
            'process_id' => request()->process_id,
            'deadline' => Carbon::parse(request()->deadline)->endOfMonth(),
            'is_ready' => 1
        ]);
    }
}
