<?php

namespace App\Http\Controllers;

use App\DataTables\ClientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Repositories\ClientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\ClientGroup;
use App\Models\Group;
use App\Models\Company;
use App\Models\Process;

class ClientController extends AppBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepo)
    {
        $this->clientRepository = $clientRepo;
    }

    /**
     * Display a listing of the Client.
     *
     * @param ClientDataTable $clientDataTable
     * @return Response
     */
    public function index(ClientDataTable $clientDataTable)
    {
        return $clientDataTable->render('clients.index');
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = \Auth::user();
        if ($currentUser->hasRole('admin')) {
            $companies = Company::where('id', $currentUser->company_id)->pluck('name', 'id')->toArray();
        } else {
            $companies = Company::all()->pluck('name', 'id')->toArray();
        }
        return view('clients.create', compact('companies'));
    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {
        $currentUser = \Auth::user();
        $input = $request->all();

        $client = $this->clientRepository->create($input);

        if (($currentUser->hasRole('admin') && $client->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        Flash::success('Client saved successfully.');

        return redirect(route('clients.index'));
    }

    /**
     * Display the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $client = $this->clientRepository->find($id);
        $currentUser = \Auth::user();

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('clients.index'));
        }

        if (($currentUser->hasRole('admin') && $client->company_id != $currentUser->company_id)
        || ($currentUser->hasRole('client') && $client->id != $currentUser->client_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        return view('clients.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit()
    {
        $client = $this->clientRepository->find(request()->client_id);
        $currentUser = \Auth::user();

        $companies = Company::all()->pluck('name', 'id')->toArray();

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('clients.index'));
        }

        if (($currentUser->hasRole('admin') && $currentUser->company_id != $client->company_id)
            || ($currentUser->hasRole('client') && $currentUser->client_id != $client->id)) {
                Flash::error('Not enough permissions for this operation');
                return redirect(route('clients.index'));
            }

        return view('clients.edit', compact('companies'))->with('client', $client);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param  int              $id
     * @param UpdateClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientRequest $request)
    {
        $client = $this->clientRepository->find($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('clients.index'));
        }

        if (($currentUser->hasRole('admin') && $client->company_id != $currentUser->company_id)
        || ($currentUser->hasRole('client') && $client->id != $currentUser->client_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        $client = $this->clientRepository->update($request->all(), $id);

        Flash::success('Client updated successfully.');

        return redirect(route('clients.index'));
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $client = $this->clientRepository->find($id);
        $currentUser = \Auth::user();

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('clients.index'));
        }

        if (($currentUser->hasRole('admin') && $client->company_id != $currentUser->company_id)) {
            Flash::error('Not enough permissions for this operation');
            return redirect(route('clients.index'));
        }

        $this->clientRepository->delete($id);

        Flash::success('Client deleted successfully.');

        return redirect(route('clients.index'));
    }

    /**
     * Show the checklist for the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function checklist($id)
    {
        $client = $this->clientRepository->find($id);

        if(empty($client)) {
            Flash::error('Client not found');

            return redirect(route('clients.index'));
        }

        $clientGroups = ClientGroup::where('client_id', $client->id)->get();

        $groups = [];
        $uniqueGroups = [];
        foreach($clientGroups as $clientGroup) {
            $group = Group::find($clientGroup->group_id);
            if(!$group->is_unique) {
                array_push($groups, $group);
            } else {
                array_push($uniqueGroups, $group);
            }
        }

        $processes = collect([]);
        foreach($groups as $group) {
            $process = Process::where('group_id', $group->id)->get();
            $processes = $processes->merge($process);
        }

        $uniqueProcesses = collect([]);
        foreach($uniqueGroups as $uniqueGroup) {
            $uniqueProcess = Process::where('group_id', $uniqueGroup->id)->get();
            $uniqueProcesses = $uniqueProcesses->merge($uniqueProcess);
        }

        foreach($processes as $process) {
            $executions = $process->executions->where('client_id', $client->id);
            if($executions) {
                $executionDates = [];
                foreach($executions as $execution) {
                    $date = $execution->deadline->format('M/y');
                    $executionDates[$date] = [
                        'is_ready' => $execution->is_ready,
                        'execution_id' => $execution->id
                    ];
                }
                $process->executions = $executionDates;
            }
        }

        foreach($uniqueProcesses as $uniqueProcess) {
            $uniqueExecutions = $uniqueProcess->executions->where('client_id', $client->id);
            if($uniqueExecutions) {
                $uniqueExecutionDetails = [];
                foreach($uniqueExecutions as $uniqueExecution) {
                    $uniqueExecutionDetails['details'] = [
                        'is_ready' => $uniqueExecution->is_ready,
                        'execution_id' => $uniqueExecution->id
                    ];
                }
                $uniqueProcess->executions = $uniqueExecutionDetails;
            }
        }

        return view('clients.checklist', compact('clientGroups', 'groups', 'processes', 'uniqueGroups', 'uniqueProcesses'))->with('client', $client);
    }
}
