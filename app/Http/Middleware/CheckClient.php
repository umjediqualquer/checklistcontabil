<?php

namespace App\Http\Middleware;

use App\Models\Client;
use Auth;
use Closure;
use Flash;

class CheckClient
{
    public function handle($request, Closure $next)
    {
        $client = Client::find($request->client_id);

        if(empty($client)) {
            Flash::error('Client not found');
            return redirect(route('clients.index'));
        }

        $request->attributes->add(['current-client' => $client]);

        return $next($request);
    }
}