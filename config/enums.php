<?php
return [
    'roles' =>
        [
            'SUPER_ADMIN'     => ['id' => 1, 'name' => 'super_admin',     'display_name' =>  'Super Administrador',     'guard_name' => 'web', 'created_at' => 'CURRENT_TIMESTAMP', 'updated_at' => 'CURRENT_TIMESTAPM'],
            'ADMIN'           => ['id' => 2, 'name' => 'admin',           'display_name' =>  'Administrador',           'guard_name' => 'web', 'created_at' => 'CURRENT_TIMESTAMP', 'updated_at' => 'CURRENT_TIMESTAPM'],
            'CLIENT'          => ['id' => 3, 'name' => 'client',          'display_name' =>  'Cliente',                 'guard_name' => 'web', 'created_at' => 'CURRENT_TIMESTAMP', 'updated_at' => 'CURRENT_TIMESTAPM'],
        ],
];