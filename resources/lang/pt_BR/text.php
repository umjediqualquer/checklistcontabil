<?php

return [

    // Actions
    'save'                  => 'Salvar',
    'cancel'                => 'Cancelar',
    'back'                  => 'Voltar',
    'add'                   => 'Adicionar',
    'remove'                => 'Remover',
    'edit'                  => 'Editar',
    'details'               => 'Detalhes',             
    'search'                => 'Pesquisar',
    
    // Messages
    'confirmation'          => 'Você tem certeza?',
    'yes'                   => 'Sim',
    'no'                    => 'Não',
    'member_since'          => 'Membro desde',

    // Menu
    'home'                  => 'Início',
    'profile'               => 'Perfil',
    'dashboard'             => 'Dashboard',
    'logout'                => 'Sair',
 
];
