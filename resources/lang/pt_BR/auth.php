<?php

return [

    'failed'                       => 'Não há usuário com essas credenciais no sistema.',
    'throttle'                     => 'Muitas tentativas. Tente novamente em :seconds segundos.',

    'sign_in'                      => 'Informe suas credenciais para realizar o login no sistema:',
    'register'                     => 'Informe os dados seguintes para cadastrar-se:',
    'enter_mail'                   => 'Informe o seu e-mail de cadastro:',
    'reset_password'               => 'Informe o seu e-mail de cadastro e a sua nova senha:',

    'success_reset_web'            => 'Sua senha foi alterada com sucesso!',
    'success_reset_api'            => 'Sua senha foi alterada com sucesso!<br/><b>Você já pode fechar essa janela.</b>',

    'full_name'                    => 'Nome Completo',
    'email'                        => 'E-mail',
    'password'                     => 'Senha',
    'confirm_password'             => 'Confirmar Senha',
    'remember'                     => 'Memorizar',

    'new_user'                     => 'Cadastrar no sistema',
    'forgot_password'              => 'Esqueci minha senha',
    'already_user'                 => 'Já sou um usuário',
    'login'                        => 'Fazer login',
    'back'                         => 'Voltar ao início',

    'button_sign_in'               => 'Entrar',
    'button_register'              => 'Registrar',
    'button_send_mail'             => 'Enviar',
    'button_reset'                 => 'Redefinir',

];
