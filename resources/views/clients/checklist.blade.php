@extends('layouts.app')
@include('flash::message')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h1 class="box-title h1">Tarefas recorrentes para {{ $client->name }}</h1>
                        <div class="box-tools pull-right"></div>
                    </div>

                    <div class="box-body">
                        @php($currentDate = Carbon\Carbon::now())
                        <ul style="list-style-type: none;">
                            @while($currentDate >= $client->created_at)
                                <li class="h2">{{ $currentDate->format('M/y') }}
                                    <ul style="list-style-type: none;">
                                        @foreach($groups as $group)
                                            <li class="h3">{{ $group->name }}
                                                <ul style="list-style-type: none;">
                                                    @foreach($processes as $process)
                                                        @if($process->group_id == $group->id)
                                                            @isset($process->executions[$currentDate->format('M/y')])
                                                                @if($process->executions[$currentDate->format('M/y')]['is_ready'])
                                                                    <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$process->id}}" deadline="{{$currentDate->format('Y-m')}}" data-id="{{$process->executions[$currentDate->format('M/y')]['execution_id']}}" checked></input>
                                                                    <li style="padding-bottom: 10px;" class="col-sm-11"><a href="">{{ $process->name }}</a></li>
                                                                @else
                                                                    <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$process->id}}" deadline="{{$currentDate->format('Y-m')}}" data-id="{{$process->executions[$currentDate->format('M/y')]['execution_id']}}"></input>
                                                                    <li style="padding-bottom: 10px;" class="col-sm-11">{{ $process->name }}</li>
                                                                @endif
                                                            @else
                                                                <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$process->id}}" deadline="{{$currentDate->format('Y-m')}}" data-id="0"></input>
                                                                <li style="padding-bottom: 10px;" class="col-sm-11">{{ $process->name }}</li>
                                                            @endisset
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @php($currentDate->subMonth())
                            @endwhile
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h1 class="box-title h1">Tarefas únicas para {{ $client->name }}</h1>
                        <div class="box-tools pull-right"></div>
                    </div>

                    <div class="box-body">
                        <ul style="list-style-type: none;">
                            @foreach($uniqueGroups as $uniqueGroup)
                                <li class="h3">{{ $uniqueGroup->name }}
                                    <ul style="list-style-type: none;">
                                        @foreach($uniqueProcesses as $uniqueProcess)
                                            @if($uniqueProcess->group_id == $uniqueGroup->id)
                                                @isset($uniqueProcess->executions['details'])
                                                    @if($uniqueProcess->executions['details']['is_ready'])
                                                        <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$uniqueProcess->id}}" deadline="{{null}}" data-id="{{$uniqueProcess->executions['details']['execution_id']}}" checked></input>
                                                        <li style="padding-bottom: 10px;" class="col-sm-11"><a href="">{{ $uniqueProcess->name }}</a></li>
                                                    @else
                                                        <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$uniqueProcess->id}}" deadline="{{null}}" data-id="{{$uniqueProcess->executions['details']['execution_id']}}"></input>
                                                        <li style="padding-bottom: 10px;" class="col-sm-11">{{ $uniqueProcess->name }}</li>
                                                    @endif
                                                @else
                                                    <input class="col-sm-1 is-ready-checkbox" type="checkbox" process-id="{{$uniqueProcess->id}}" deadline="{{null}}" data-id="0"></input>
                                                    <li style="padding-bottom: 10px;" class="col-sm-11">{{ $uniqueProcess->name }}</li>
                                                @endisset
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(document).on("change", ".is-ready-checkbox", function() {
                clientId = @json(request()->get('current-client')->id);
                processId = $(this).attr('process-id');
                executionId = $(this).attr('data-id');
                scheduledTo = $(this).attr('deadline');

                if($(this).prop('checked')) {
                    if(executionId == 0) {
                        route = "{{ route('processes.createExecution', [':client_id', ':process_id']) }}" + "?deadline=" + scheduledTo;
                        route = route.replace(":client_id", clientId).replace(":process_id", processId);
                    }
                    else {
                        route = "{{ route('processes.toggleIsReady', [':client_id', ':process_id', ':execution_id']) }}" + "?deadline=" + scheduledTo;
                        route = route.replace(":client_id", clientId).replace(":process_id", processId).replace(":execution_id", executionId);
                    }
                } else {
                    route = "{{ route('processes.toggleIsReady', [':client_id', ':process_id', ':execution_id']) }}" + "?deadline=" + scheduledTo;
                    route = route.replace(":client_id", clientId).replace(":process_id", processId).replace(":execution_id", executionId);
                }

                $.ajax({
                    url: route,
                    dataType: "json",
                    type: "GET",
                    beforeSend: function() {
                    },
                    success: function(response) {
                    },
                    error: function(fail) {
                    },
                    complete: function() {
                        // location.reload();
                    }
                });
            });
        });
    </script>
@endpush