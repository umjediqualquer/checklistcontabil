<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>Checklist Contábil</title>

    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Bootstrap Toggle v2.2.0 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.min.css') }}">

    <!-- Font Awesome v5.12.1 -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.css') }}">

    <!-- iCheck v1.0.2 -->
    <link rel="stylesheet" href="{{ asset('vendor/icheck/skins/all.css') }}">

    <!-- AdminLTE v2.4.2 -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">

    <!-- Ionicons v2.0.0 -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    <!-- Select2 v4.0.5 -->
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">

    <!-- Datetimepicker v4.7.14 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

    @stack('css')
</head>

<body class="skin-blue sidebar-mini">
    <!-- User Picture -->
    @if(isset(Auth::user()->photo))
        @php( $profile_picture_path = Auth::user()->photo )
    @else
        @php( $profile_picture_path = "/images/default/no_user.png" )
    @endif

    <div class="wrapper">
        <!-- Main Header -->
        @include('layouts.navbar')

        <!-- Sidebar -->
        @include('layouts.sidebar')

        <!-- Content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Footer -->
        <footer class="main-footer" style="max-height:100px; text-align:center">
            <strong>Copyright © <?php echo date("Y"); ?> <a href="https://pandoapps.com.br/" target="_blank">Pandô APPs</a></strong>
        </footer>
    </div>

    <!-- jQuery v3.2.1 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Bootstrap v3.3.7 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Bootstrap Toggle v2.2.0 -->
    <script src="{{ asset('js/bootstrap-toggle.min.js') }}"></script>

    <!-- AdminLTE v2.4.2 -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>

    <!-- iCheck v1.0.2 -->
    <script src="{{ asset('vendor/icheck/icheck.min.js') }}"></script>

    <!-- Select2 v4.0.5 -->
    <script src="{{ asset('js/select2.min.js') }}"></script>

    <!-- Moment v2.15.1 -->
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/moment-ptbr.js') }}"></script>
    
    <!-- Datetimepicker v4.7.14 -->
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- Inputmask v4.0.3-b1 -->
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>

    <!-- Custom masks and datetimepicker patterns -->
    <script src="{{ asset('js/custom-masks.js') }}"></script>

    <!-- Custom Scripts -->
    <script type="text/javascript">
        // Bind mask events on load for edit pages, otherwise removeMaskOnSubmit won't trigger if user doesn't focus on input
        @if(Request::is('*edit*'))
            $(document).ready(function() {
                $(".float-mask").focusWithoutScrolling();
                $(".double-mask").focusWithoutScrolling();
                $(".money-mask").focusWithoutScrolling();
                $(".percentage-mask").focusWithoutScrolling();
                $(".integer-mask").focusWithoutScrolling();
                $(".zero-to-ten-mask").focusWithoutScrolling();
                $(".latitude-mask").focusWithoutScrolling();
                $(".longitude-mask").focusWithoutScrolling();
                $(".document-mask").focusWithoutScrolling();
                $(".national-id-mask").focusWithoutScrolling();
                $(".phone-mask").focusWithoutScrolling();
                $(".date-mask").focusWithoutScrolling();
                $(".time-mask").focusWithoutScrolling();
                $(".time-with-seconds-mask").focusWithoutScrolling();
                $(".datetime-mask").focusWithoutScrolling();
                $(".vehicle-plate-mask").focusWithoutScrolling();
                $(".zipcode-mask").focusWithoutScrolling();
                $(".state-mask").focusWithoutScrolling();
                $('input').blur(); 
            });
        @endif

        // Focus without scrolling
        $.fn.focusWithoutScrolling = function(){
            var x = window.scrollX, y = window.scrollY;
            this.focus();
            window.scrollTo(x, y);
            return this; 
        };

        // Tooltip
        $(document).tooltip({
            selector: '[data-toggle="tooltip"]',
            trigger: 'hover'
        });

        // Toggle logo on menu toggle
        function toggleLogo() {
            if ($(document).width()>=768) {
                // Use this if no icon
                $("#sidebar-logo").toggle();

                // Use this if has icon
                // if ($(document.body).hasClass('sidebar-collapse')) {
                //     $("#sidebar-logo").removeClass("navbar-logo-image-icon").attr("src", "/images/default/LOGO_PLACEHOLDER.png");
                // } else {
                //     $("#sidebar-logo").addClass("navbar-logo-image-icon").attr("src", "/images/default/ICON_PLACEHOLDER.png");
                // }
            }
        }
        $(window).on('resize', function(){
            // Use this if no icon
            if($(document).width()<768){
                $("#sidebar-logo").show();
            } else if($(document).width()>=768 && $(document.body).hasClass('sidebar-collapse')){
                $("#sidebar-logo").hide();
            }

            // Use this if has icon
            // if ($(document).width()<768) {
            //     $("#sidebar-logo").removeClass("navbar-logo-image-icon").attr("src", "/images/default/LOGO_PLACEHOLDER.png");
            // } else if($(document).width()>=768 && $(document.body).hasClass('sidebar-collapse')) {
            //     $("#sidebar-logo").addClass("navbar-logo-image-icon").attr("src", "/images/default/ICON_PLACEHOLDER.png");
            // }
        });

        // Preview upload image
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var id = '#' + input.attributes.id.value + '-img';
                    $(id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('input[type=file]').change(function(){
            readURL(this);
            $("#"+this.id+"-img").addClass('thumbnail');
        });

        // AJAX flash message
        $(document).ready(function() {
            $(".content").prepend("<div class='alert-ajax'></div>");
        });

        // Allow uncheck radio button
        $('input:radio').bind('click mousedown', (function() {
            var isChecked;
            return function(event) {
                if(event.type == 'click') {
                    if(isChecked) {
                        isChecked = this.checked = false;
                    } else {
                        isChecked = true;
                    }
                } else {
                    isChecked = this.checked;
                }
            }
        })());

        // Menu filter
        function filterMenu() {
            var input, filter, ul, li, a, i;
            input = document.getElementById('inputSearch');
            
            if(input.value !== ""){
                filter = input.value.toUpperCase();
                ul = document.getElementById("sidebarMenu");
                li = ul.getElementsByTagName('li');

                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    if (a !== undefined && a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            } else {
                ul = document.getElementById("sidebarMenu");
                li = ul.getElementsByTagName('li');
                for (i = 0; i < li.length; i++) {
                    li[i].style.display = "";
                }
            }
        }

        // Convert \n to <br>
        function nl2br(str, is_xhtml) {
            var breakTag = '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }

        // Loading overlay
        function showLoading() {
            $.LoadingOverlay("show", {
                // image            : '',
                imageColor       : '#ccc',
                // text             : customText,
                // textResizeFactor : 0.2,
                // textColor        : '#fff',
                background       : 'rgba(0, 0, 0, 0.5)',
                fade             : [200, 200],
            });
        }
        function hideLoading() {
            $.LoadingOverlay("hide");
        }

        // Format text as money
        function formatMoney(value){
            return parseFloat(value).toLocaleString('pt-BR', { minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'BRL' });
        }

        // Format text as percentage
        function formatPercentage(value){
            return parseFloat(value).toLocaleString('pt-BR', { minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'percent' });
        }

        // Format text as decimal
        function formatDecimal(value){
            return parseFloat(value).toLocaleString('pt-BR', { minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'decimal' });
        }

        // Format text as integer
        function formatInteger(value){
            return parseInt(value).toLocaleString('pt-BR', { minimumFractionDigits: 0, maximumFractionDigits: 0, style: 'decimal' });
        }

        // iCheck
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
            });
        });
        $('input').on('ifChanged', function (event) {
            $(event.target).trigger('change');
        });
    </script>

    @stack('js')
</body>
</html>