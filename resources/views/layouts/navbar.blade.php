<header class="main-header">
    @php( $sidebar_logo = "/images/default/LOGO_PLACEHOLDER.png" )

    <!-- Left Logo --> 
    <a href="{{ route('home') }}" class="logo">
        <img id="sidebar-logo" src="{{ $sidebar_logo }}" class="navbar-logo-image" alt="Checklist Contábil"/>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar Toogle Button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" onClick="toggleLogo()">
            <i class="fas fa-bars" style="font-size:13px;"></i>
        </a>

        <!-- Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">

                    <!-- User Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ $profile_picture_path }}" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><b>{{ Auth::user()->name }}</b></span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- User Image -->
                        <li class="user-header">
                            <img src="{{ $profile_picture_path }}" class="img-circle" alt="User Image"/>
                            <p>
                                <b>{{ Auth::user()->name }}</b>
                                <small>{{ \Lang::choice('text.member_since','p') }} {{ Auth::user()->created_at->format('d/m/Y') }}</small>
                            </p>
                        </li>
                        
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('home') }}" class="btn btn-default btn-flat">{{ \Lang::choice('text.home','p') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ \Lang::choice('text.logout','p') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </div>
                        </li>
                    </ul>
                    
                </li>
            </ul>
        </div>
    </nav>
</header>