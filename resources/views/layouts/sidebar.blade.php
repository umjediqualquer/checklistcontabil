<aside class="main-sidebar" id="sidebar-wrapper">
    <section class="sidebar">
        <!-- User Panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ $profile_picture_path }}" class="img-circle" alt="User Image" style="max-width: 50px !important;"/>
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name}}</p>
                <small>{{ Auth::user()->roles->first()->display_name }}</small>
            </div>
        </div>

        <!-- Search Form -->
        <form action="#" class="sidebar-form">
            <div class="input-group">
                <input type="text" id="inputSearch" class="form-control" placeholder="{{ \Lang::get('text.search') }}" onkeyup="filterMenu()"/>
                <span class="input-group-btn">
                    <button name='search' id='search-btn' class="btn btn-flat" style="cursor:default"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>

        <!-- Menu -->
        <ul class="sidebar-menu tree" data-widget="tree" id="sidebarMenu">
            @include('layouts.menu')
        </ul>
    </section>
</aside>