@role('super_admin')
    <li class="{{ Request::is('companies*') ? 'active' : '' }}">
        <a href="{{ route('companies.index') }}"><i class="fas fa-edit"></i><span>Companies</span></a>
    </li>
@endrole

<li class="{{ Request::is('clients*') ? 'active' : '' }}">
    <a href="{{ route('clients.index') }}"><i class="fas fa-edit"></i><span>Clients</span></a>
</li>

@if(\Auth::user()->hasRole('super_admin') || \Auth::user()->hasRole('admin'))
    <li class="{{ Request::is('groups*') ? 'active' : '' }}">
        <a href="{{ route('groups.index') }}"><i class="fas fa-edit"></i><span>Groups</span></a>
    </li>

    <li class="{{ Request::is('clientGroups*') ? 'active' : '' }}">
        <a href="{{ route('clientGroups.index') }}"><i class="fas fa-edit"></i><span>Client Groups</span></a>
    </li>

    <li class="{{ Request::is('processes*') ? 'active' : '' }}">
        <a href="{{ route('processes.index') }}"><i class="fas fa-edit"></i><span>Processes</span></a>
    </li>

    <li class="{{ Request::is('executions*') ? 'active' : '' }}">
        <a href="{{ route('executions.index') }}"><i class="fas fa-edit"></i><span>Executions</span></a>
    </li>

    <li class="{{ Request::is('attachments*') ? 'active' : '' }}">
        <a href="{{ route('attachments.index') }}"><i class="fas fa-edit"></i><span>Attachments</span></a>
    </li>
@endif

<li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="fas fa-sign-out-alt sidebar-icons"></i><span>{{ \Lang::get('text.logout') }}</span>
    </a>
</li>