@extends('auth.template')

@section('content')
    <p class="login-box-msg">{{ \Lang::get('auth.enter_mail') }}</p>

    @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form method="post" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}

        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ \Lang::get('auth.email') }}">
            <span class="fas fa-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('email')) }}</strong>
                </span>
            @endif
        </div>

        <div class="row">
            <div class="col-xs-5" style="margin-top:7px">
                <a href="{{ route('login') }}">{{ \Lang::get('auth.back') }}</a>
            </div>

            <div class="col-xs-7">
                <button type="submit" class="btn btn-primary btn-flat pull-right">
                    <i class="fas fa-envelope" style="margin-right:5px"></i> {{ \Lang::get('auth.button_send_mail') }}
                </button>
            </div>
        </div>
    </form>
@endsection