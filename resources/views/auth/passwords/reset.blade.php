@extends('auth.template')

@section('content')
    <p class="login-box-msg">{{ \Lang::get('auth.reset_password') }}</p>

    <form method="post" action="{{ url('/password/reset') }}">
        {!! csrf_field() !!}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ \Lang::get('auth.email') }}">
            <span class="fas fa-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('email')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" name="password" placeholder="{{ \Lang::get('auth.password') }}">
            <span class="fas fa-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('password')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ \Lang::get('auth.confirm_password') }}">
            <span class="fas fa-lock form-control-feedback"></span>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('password_confirmation')) }}</strong>
                </span>
            @endif
        </div>

        <div class="row">
            <div class="col-xs-5" style="margin-top:7px">
                <a href="{{ route('login') }}">{{ \Lang::get('auth.back') }}</a>
            </div>

            <div class="col-xs-7">
                <button type="submit" class="btn btn-primary btn-flat pull-right">
                    <i class="fas fa-sync-alt" style="margin-right:5px"></i> {{ \Lang::get('auth.button_reset') }}
                </button>
            </div>
        </div>
    </form>
@endsection
