@extends('auth.template')

@section('content')
    <p class="login-box-msg">{{ \Lang::get('auth.sign_in') }}</p>

    <form method="post" action="{{ route('login') }}">
        {!! csrf_field() !!}

        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ \Lang::get('auth.email') }}">
            <span class="fas fa-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('email')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" placeholder="{{ \Lang::get('auth.password') }}" name="password">
            <span class="fas fa-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('password')) }}</strong>
                </span>
            @endif

        </div>

        <div class="row">
            <div class="col-xs-7">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="remember"><span style="margin-left:10px">{{ \Lang::get('auth.remember') }}</span>
                    </label>
                </div>
            </div>

            <div class="col-xs-5">
                <button type="submit" class="btn btn-primary btn-flat pull-right">
                    <i class="fas fa-sign-in-alt" style="margin-right:5px"></i> {{ \Lang::get('auth.button_sign_in') }}
                </button>
            </div>
        </div>
    </form>

    <a href="{{ url('/password/reset') }}">{{ \Lang::get('auth.forgot_password') }}</a><br>
@endsection
