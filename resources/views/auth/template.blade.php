<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Checklist Contábil</title>

    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Font Awesome v5.12.1 -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.css') }}">

    <!-- Ionicons v2.0.0 -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    <!-- AdminLTE v2.4.2 -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">

    <!-- iCheck v1.0.2 -->
    <link rel="stylesheet" href="{{ asset('vendor/icheck/skins/all.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/custom_auth.css') }}">

    @stack('css')
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <img class="logo-img" src="/images/default/ICON_PLACEHOLDER.png" alt="Checklist Contábil"/>
        </div>

        <div class="login-box-body">
            @include('flash::message')

            @yield('content')
        </div>
    </div>
    
    <!-- jQuery v3.2.1 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Bootstrap v3.3.7 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- AdminLTE v2.4.2 -->
    <script src="{{ asset('js/adminlte.min.js') }}"></script>

    <!-- iCheck v1.0.2 -->
    <script src="{{ asset('vendor/icheck/icheck.min.js') }}"></script>

    <!-- Custom Scripts -->
    <script type="text/javascript">
        // iCheck
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
            });
        });
        $('input').on('ifChanged', function (event) {
            $(event.target).trigger('change');
        });
    </script>

    @stack('scripts')
</body>
</html>
