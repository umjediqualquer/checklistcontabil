@extends('auth.template')

@section('content')
    <p class="login-box-msg">{{ \Lang::get('auth.register') }}</p>

    <form method="post" action="{{ url('/register') }}">
        {!! csrf_field() !!}

        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="{{ \Lang::get('auth.full_name') }}">
            <span class="fas fa-user form-control-feedback"></span>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('name')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ \Lang::get('auth.email') }}">
            <span class="fas fa-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('email')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" name="password" placeholder="{{ \Lang::get('auth.password') }}">
            <span class="fas fa-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('password')) }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ \Lang::get('auth.confirm_password') }}">
            <span class="fas fa-lock form-control-feedback"></span>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ strip_tags($errors->first('password_confirmation')) }}</strong>
                </span>
            @endif
        </div>

        <div class="row">
            <div class="col-xs-7" style="margin-top:7px">
                <a href="{{ route('login') }}">{{ \Lang::get('auth.already_user') }}</a>
            </div>

            <div class="col-xs-5">
                <button type="submit" class="btn btn-primary btn-flat pull-right">
                    <i class="fas fa-user-plus" style="margin-right:5px"></i> {{ \Lang::get('auth.button_register') }}
                </button>
            </div>
        </div>
    </form>
@endsection
