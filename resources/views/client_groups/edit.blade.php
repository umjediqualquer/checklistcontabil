@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Client Group
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($clientGroup, ['route' => ['clientGroups.update', $clientGroup->id], 'method' => 'patch']) !!}

                        @include('client_groups.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection