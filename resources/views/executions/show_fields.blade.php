<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $execution->id }}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', 'Client Id:') !!}
    <p>{{ $execution->client_id }}</p>
</div>

<!-- Process Id Field -->
<div class="form-group">
    {!! Form::label('process_id', 'Process Id:') !!}
    <p>{{ $execution->process_id }}</p>
</div>

<!-- Is Ready Field -->
<div class="form-group">
    {!! Form::label('is_ready', 'Is Ready:') !!}
    <p>{{ $execution->is_ready }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $execution->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $execution->updated_at }}</p>
</div>

