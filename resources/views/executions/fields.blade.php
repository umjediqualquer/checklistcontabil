<!-- Client Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', 'Client Id:') !!}
    {!! Form::select('client_id', ['id' => 'Client'] + $clients, null, ['class' => 'form-control']) !!}
</div>

<!-- Process Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('process_id', 'Process Id:') !!}
    {!! Form::select('process_id', ['id' => 'Process'] + $processes, null, ['class' => 'form-control']) !!}
</div>

<!-- Is Ready Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_ready', 'Is Ready:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_ready', 0) !!}
        {!! Form::checkbox('is_ready', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('executions.index') }}" class="btn btn-default">Cancel</a>
</div>
