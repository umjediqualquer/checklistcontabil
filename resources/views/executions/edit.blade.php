@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Execution
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($execution, ['route' => ['executions.update', $execution->id], 'method' => 'patch']) !!}

                        @include('executions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection