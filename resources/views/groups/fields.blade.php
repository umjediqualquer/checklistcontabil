<!-- Company Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_id', 'Company Id:') !!}
    {!! Form::select('company_id', ['id' => 'Company'] + $companies, null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Unique Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_unique', 'Is Unique:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_unique', 0) !!}
        {!! Form::checkbox('is_unique', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('groups.index') }}" class="btn btn-default">Cancel</a>
</div>
