<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        $roles = array_map(['App\Helpers\Functions', 'addTimestamp'], array_values(config('enums.roles')));
        DB::table('roles')->insert($roles);

        // Companies
        $companies = [
            [
                'name' => 'Stark Importação',
                'address' => 'Quadra 31, 864',
                'cnpj' => '87839169000118',
                'phone' => '3529938477',
                'email' => 'stark_importacao@marvel.com',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'name' => 'HighClass Cap',
                'address' => 'Rua Elza de Souza Machado, 263',
                'cnpj' => '38671168000100',
                'phone' => '3577583326',
                'email' => 'highclass_cap@marvel.com',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ];
        DB::table('companies')->insert($companies);

        // Clients
        $clients = [
            [
                'company_id' => 1,
                'name' => 'Vitor Benício Barros',
                'birth_date' => DB::raw('CURRENT_TIMESTAMP'),
                'rg' => '11815724870',
                'phone' => '5125189524',
                'address' => 'Rua Arizona, 164',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 1,
                'name' => 'Aurora Gabriela Nunes',
                'birth_date' => DB::raw('CURRENT_TIMESTAMP'),
                'rg' => '489810809',
                'phone' => '85993865703',
                'address' => 'Rua 204, 918',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 2,
                'name' => 'Benedita Pietra Heloise Brito',
                'birth_date' => DB::raw('CURRENT_TIMESTAMP'),
                'rg' => '358670743',
                'phone' => '33999058615',
                'address' => 'Rua Cícero Siqueira, 332',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ];
        DB::table('clients')->insert($clients);

        // Users
        $users = [
            [
                'company_id' => null,
                'client_id' => null,
                'name' => 'Super Admin',
                'email' => 'super@admin.com',
                'password' => bcrypt('123456'),
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 1,
                'client_id' => null,
                'name' => 'Tony Stark',
                'email' => 'tony@starkindustries.com',
                'password' => bcrypt('123456'),
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 1,
                'client_id' => 1,
                'name' => 'Pepper Potts',
                'email' => 'pepper@starkindustries.com',
                'password' => bcrypt('123456'),
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ];
        DB::table('users')->insert($users);

        // User Roles
        $userRoles = [
            [
                'role_id' => config('enums.roles.SUPER_ADMIN.id'),
                'model_type' => 'App\User',
                'model_id' => 1
            ],
            [
                'role_id' => config('enums.roles.ADMIN.id'),
                'model_type' => 'App\User',
                'model_id' => 2
            ],
            [
                'role_id' => config('enums.roles.CLIENT.id'),
                'model_type' => 'App\User',
                'model_id' => 3
            ]
        ];
        DB::table('model_has_roles')->insert($userRoles);

        // Groups
        $groups = [
            [
                'company_id' => 1,
                'name' => 'Grupo recorrente Stark',
                'is_unique' => 0,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 1,
                'name' => 'Grupo único Stark',
                'is_unique' => 1,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 2,
                'name' => 'Grupo recorrente Cap',
                'is_unique' => 0,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'company_id' => 2,
                'name' => 'Grupo único Cap',
                'is_unique' => 1,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ];
        DB::table('groups')->insert($groups);

        // Processes
        $processes = [
            [
                'group_id' => 1,
                'name' => 'Processo 1 - Stark_R',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 1,
                'name' => 'Processo 2 - Stark_R',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 2,
                'name' => 'Processo 1 - Stark_U',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 2,
                'name' => 'Processo 2 - Stark_U',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 3,
                'name' => 'Processo 1 - Cap_R',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 3,
                'name' => 'Processo 2 - Cap_R',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 4,
                'name' => 'Processo 1 - Cap_U',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'group_id' => 4,
                'name' => 'Processo 2 - Cap_U',
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ];
        DB::table('processes')->insert($processes);

        // Client Groups
        $clientGroups = [
            [
                'client_id' => 1,
                'group_id' => 1,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'client_id' => 1,
                'group_id' => 2,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'client_id' => 2,
                'group_id' => 2,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'client_id' => 3,
                'group_id' => 3,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'client_id' => 3,
                'group_id' => 4,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
        ];
        DB::table('client_groups')->insert($clientGroups);
    }
}