<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ClientGroup;
use Faker\Generator as Faker;

$factory->define(ClientGroup::class, function (Faker $faker) {

    return [
        'client_id' => $faker->randomDigitNotNull,
        'group_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
