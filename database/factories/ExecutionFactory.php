<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Execution;
use Faker\Generator as Faker;

$factory->define(Execution::class, function (Faker $faker) {

    return [
        'client_id' => $faker->randomDigitNotNull,
        'process_id' => $faker->randomDigitNotNull,
        'is_ready' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
